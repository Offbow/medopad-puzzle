package com.medopad.puzzle

import android.app.Application
import android.os.Looper
import com.medopad.puzzle.arch.AppModule
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import net.danlew.android.joda.JodaTimeAndroid

import timber.log.Timber

open class PuzzleApp : Application() {
    override fun onCreate() {
        AppModule.application = this
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        JodaTimeAndroid.init(this)

        //Reportedly improves the speed of RX by allowing RX to be executed outside of Android 16ms Main thread frame
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { AndroidSchedulers.from(Looper.getMainLooper(), true) }
    }
}
