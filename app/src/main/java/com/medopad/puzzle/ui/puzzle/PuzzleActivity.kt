package com.medopad.puzzle.ui.puzzle

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.medopad.puzzle.R
import com.medopad.puzzle.arch.BaseActivity
import com.medopad.puzzle.arch.Launcher
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearchBoardNode
import kotlinx.android.synthetic.main.activity_puzzle.*

class PuzzleActivity : BaseActivity<PuzzleMvp.Presenter>(), PuzzleMvp.View {

    private val solutionListAdapter: SolutionAdapter by lazy { SolutionAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_puzzle)
        presenter = PuzzlePresenter(this)
        presenter.onCreate()

        //a wake lock, only needed for this example but normally only on when needed
        puzzle_basic_title.keepScreenOn = true

        puzzle_basic_action.setOnClickListener { presenter.onPuzzleTapped() }
        puzzle_solver_simple_action.setOnClickListener { presenter.onSimpleSolveTapped() }
        puzzle_solver_comprehensive_action.setOnClickListener { presenter.onComprehensiveSolveTapped() }
        puzzle_solver_list.adapter = solutionListAdapter
    }

    override fun setSlidingPuzzle(puzzleMap: String) {
        puzzle_basic_print_out.text = puzzleMap
    }

    override fun setPuzzleSolutionNoFound() {
        AlertDialog.Builder(this)
                .setTitle(R.string.puzzle_solver_error_title)
                .setMessage(R.string.puzzle_solver_error_body)
                .setPositiveButton(R.string.puzzle_solver_error_action, null)
                .apply {
                    show()
                }
    }

    override fun setPuzzleSolution(nodes: List<BreadthFirstSearchBoardNode>, millisTaken: Long) {
        puzzle_solver_info.text = getString(R.string.puzzle_solver_info_format, nodes.size, millisTaken)
        solutionListAdapter.setSolutionList(nodes)
    }

    override fun showSolverProgress(show: Boolean) {
        puzzle_solver_content_group.visibility = if (show) View.GONE else View.VISIBLE
        puzzle_solver_progress_group.visibility = if (show) View.VISIBLE else View.GONE
    }

    companion object {

        @JvmStatic
        fun start(launcher: Launcher) {
            val intent = Intent(launcher.context(), PuzzleActivity::class.java)
            launcher.launch(intent)
        }
    }
}
