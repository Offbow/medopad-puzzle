package com.medopad.puzzle.ui.splash

import com.medopad.puzzle.arch.BasePresenter

class SplashPresenter constructor(view: SplashMvp.View) : BasePresenter<SplashMvp.View>(view), SplashMvp.Presenter {
    override fun onCreate() {
        view?.startPuzzleScreen()
    }
}
