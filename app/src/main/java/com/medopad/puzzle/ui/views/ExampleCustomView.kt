package com.medopad.puzzle.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View

class ExampleCustomView : View {
    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet?, resStyle: Int) : super(context, attrs, resStyle) {

    }
}
