package com.medopad.puzzle.ui.views

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.os.HandlerCompat.postDelayed
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.medopad.puzzle.R
import com.medopad.puzzle.ext.android.getCompatColor
import kotlinx.android.synthetic.main.element_animate_end.view.*

class ExampleConstraintAnimationView : ConstraintLayout {
    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    }

    constructor(context: Context, attrs: AttributeSet?, resStyle: Int) : super(context, attrs, resStyle) {
    }

    private val baseView = LayoutInflater.from(context).inflate(R.layout.element_animate_start, this, false) as ConstraintLayout

    val animationDuration: Long = 500 // Animation duration

    var backgroundToTrans = ValueAnimator() // Value Animator for background colours
    var backgroundToColor = ValueAnimator()

    // setup for hiding and showing text of the three buttons
    private val childTextViews: List<TextView>
    var textColor: ColorStateList
    var textColorTrans = ColorStateList(
            arrayOf(),
            intArrayOf())

    init {
        //setup the view
        val set = ConstraintSet()

        addView(baseView)
        set.clone(this)
        set.match(baseView, this)

        //setup the text views
        childTextViews = (0..baseView.childCount)
                .map { baseView.getChildAt(it) }
                .filter { it is TextView }
                .map { it as TextView }

        textColor = childTextViews[0].textColors

        //setup the background color animators
        backgroundToTrans = ValueAnimator.ofObject(ArgbEvaluator(), context.getCompatColor(R.color.color_grey_background), context.getCompatColor(R.color.color_grey_background_trans))
        backgroundToTrans.duration = animationDuration
        backgroundToColor = ValueAnimator.ofObject(ArgbEvaluator(), context.getCompatColor(R.color.color_grey_background_trans), context.getCompatColor(R.color.color_grey_background))
        backgroundToColor.duration = animationDuration
    }

    fun showView() {
        //set text to original text color
        childTextViews.forEach {
            it.setTextColor(textColor)
        }
        transitionTo(R.layout.element_animate_end, backgroundToColor)

        visibility = View.VISIBLE
    }

    fun hideView() {
        //set text to transparent
        childTextViews.forEach {
            it.setTextColor(textColorTrans)
        }

        transitionTo(R.layout.element_animate_start, backgroundToTrans)

        postDelayed({
            visibility = View.INVISIBLE
        }, animationDuration)
    }

    private fun transitionTo(@LayoutRes layoutRes: Int, backgroundAnimator: ValueAnimator) {
        //background color animation
        backgroundAnimator.addUpdateListener { animator -> setBackgroundColor(animator.animatedValue as Int) }
        backgroundAnimator.start()

        //clone the layout that being changed to
        val set = ConstraintSet()
        set.clone(context, layoutRes)

        //setup animation
        val changeBounds = ChangeBounds()
        changeBounds.duration = animationDuration
        //changeBounds.interpolator = OvershootInterpolator()//Edit the animation

        //apply the animation
        TransitionManager.beginDelayedTransition(baseView, changeBounds)
        set.applyTo(baseView)
    }

    fun setDismissListener(clickListener: ((View) -> Unit)?) {
        info_layout?.setOnClickListener { _ ->
            clickListener?.invoke(info_layout)
        }
        info_close?.setOnClickListener { _ ->
            clickListener?.invoke(info_close)
        }
    }
}

fun ConstraintSet.match(view: View, toParent: View) {
    this.connect(view.id, ConstraintSet.TOP, toParent.id, ConstraintSet.TOP)
    this.connect(view.id, ConstraintSet.START, toParent.id, ConstraintSet.START)
    this.connect(view.id, ConstraintSet.END, toParent.id, ConstraintSet.END)
    this.connect(view.id, ConstraintSet.BOTTOM, toParent.id, ConstraintSet.BOTTOM)
}
