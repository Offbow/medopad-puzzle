package com.medopad.puzzle.ui.puzzle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.medopad.puzzle.R
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearchBoardNode
import kotlinx.android.synthetic.main.element_solution_step.view.*

class SolutionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var solutionList: List<BreadthFirstSearchBoardNode> = listOf()

    init {
        //for better animations
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SolutionViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.element_solution_step, parent, false))
    }

    override fun getItemCount(): Int = solutionList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        solutionList[position].let { node ->
            if (node.parentNode != null) {
                holder.itemView.puzzle_solution_list_title.text = holder.itemView.context.getString(R.string.puzzle_solver_list_title, node.move?.name, node.distance, node.blockID)
            } else {
                holder.itemView.puzzle_solution_list_title.text = holder.itemView.context.getString(R.string.puzzle_solver_list_title_root)
            }

            holder.itemView.puzzle_solution_list_map.text = BoardMapUtil.formatBoardMap(node.getMap())
        }
    }

    fun setSolutionList(newSolutionList: List<BreadthFirstSearchBoardNode>) {
        val calculatedDiff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return solutionList[oldItemPosition].getBoardHash() == newSolutionList[newItemPosition].getBoardHash()
            }

            override fun getOldListSize(): Int = solutionList.size

            override fun getNewListSize(): Int = newSolutionList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return solutionList[oldItemPosition].getBoardHash() == newSolutionList[newItemPosition].getBoardHash()
            }

        })
        this.solutionList = newSolutionList
        calculatedDiff.dispatchUpdatesTo(this)
    }

    open inner class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class SolutionViewHolder(itemView: View) : BaseViewHolder(itemView)
}