package com.medopad.puzzle.ui.puzzle

import com.medopad.puzzle.arch.BasePresenter
import com.medopad.puzzle.slidingpuzzle.SlidingGame
import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearch
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearchBoardNode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class PuzzlePresenter constructor(view: PuzzleMvp.View) : BasePresenter<PuzzleMvp.View>(view), PuzzleMvp.Presenter {

    var count = 0

    var slidingPuzzleGame = SlidingGame()

    override fun onCreate() {
        printSlidingPuzzle(slidingPuzzleGame.getBoardMap())
    }

    override fun onPuzzleTapped() {
        when (count) {
            0 -> slidingPuzzleGame.performMove(GameMove.DOWN, 1, 8)
            1 -> slidingPuzzleGame.performMove(GameMove.DOWN, 1, 9)
            2 -> slidingPuzzleGame.performMove(GameMove.DOWN, 1, 7)
            3 -> slidingPuzzleGame.performMove(GameMove.UP, 1, 7)
            4 -> slidingPuzzleGame.performMove(GameMove.UP, 1, 7)
            5 -> slidingPuzzleGame.performMove(GameMove.UP, 1, 8)
            6 -> slidingPuzzleGame.performMove(GameMove.UP, 1, 9)
            7 -> slidingPuzzleGame.performMove(GameMove.RIGHT, 1, 10)
            8 -> slidingPuzzleGame.performMove(GameMove.LEFT, 1, 10)
            9 -> slidingPuzzleGame.performMove(GameMove.LEFT, 1, 11)
            10 -> slidingPuzzleGame.performMove(GameMove.LEFT, 1, 10)
            11 -> slidingPuzzleGame.performMove(GameMove.RIGHT, 1, 11)
            else -> count = -1
        }
        count++

        printSlidingPuzzle(slidingPuzzleGame.getBoardMap())
    }

    private fun printSlidingPuzzle(map: List<List<Block?>>) {

        view?.setSlidingPuzzle(BoardMapUtil.formatBoardMap(map))
    }

    override fun onSimpleSolveTapped() {
        val startTime = System.currentTimeMillis()
        val breadthFirstSearch = BreadthFirstSearch()
        view?.showSolverProgress(true)

        breadthFirstSearch.findSolutionAsSingle(solverBlocks)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.showSolverProgress(false)
                    Timber.d("Simple Solutions founds, steps ${it.size}}")
                    displaySolution(it, startTime)
                }, {
                    Timber.e(it)
                    view?.showSolverProgress(false)
                    view?.setPuzzleSolutionNoFound()
                }).addTo(subscription)
    }

    override fun onComprehensiveSolveTapped() {
        val startTime = System.currentTimeMillis()
        val breadthFirstSearch = BreadthFirstSearch()
        view?.showSolverProgress(true)

        breadthFirstSearch.findSolutionWithAllPermutationsAsSingle(solverBlocks)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("Solutions founds ${it.size} \n found solutions lengths ${it.map { it.size }}")
                    displaySolution(it.minBy { it.size }, startTime)
                }, {
                    Timber.e(it)
                    view?.showSolverProgress(false)
                    view?.setPuzzleSolutionNoFound()
                }).addTo(subscription)
    }

    private fun displaySolution(solutionList: List<BreadthFirstSearchBoardNode>?, startTime: Long) {
        view?.showSolverProgress(false)

        if (solutionList.isNullOrEmpty().not()) {
            view?.setPuzzleSolution(solutionList!!, System.currentTimeMillis() - startTime)
        } else {
            view?.setPuzzleSolutionNoFound()
        }
    }


    //duplicate list from SlidingGame
    val solverBlocks = listOf(
            Block(Block.WALL_ID, 1, 6, 0, 0, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 6, 1, 0, 1, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 6, 1, 5, 1, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 1, 1, 1, 6, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 1, 1, 4, 6, 'X', Block.BlockType.WALL),
            Block(Block.GOAL_ID, 1, 2, 2, 6, 'Z', Block.BlockType.WALL_GOAL),
            Block(3, 2, 1, 1, 1, 'a', Block.BlockType.PIECE),
            Block(4, 2, 1, 4, 1, 'c', Block.BlockType.PIECE),
            Block(5, 2, 1, 1, 3, 'd', Block.BlockType.PIECE),
            Block(6, 2, 1, 4, 3, 'f', Block.BlockType.PIECE),
            Block(7, 1, 2, 2, 3, 'e', Block.BlockType.PIECE),
            Block(8, 1, 1, 2, 4, 'g', Block.BlockType.PIECE),
            Block(9, 1, 1, 3, 4, 'h', Block.BlockType.PIECE),
            Block(10, 1, 1, 1, 5, 'i', Block.BlockType.PIECE),
            Block(11, 1, 1, 4, 5, 'j', Block.BlockType.PIECE),
            Block(12, 2, 2, 2, 1, 'b', Block.BlockType.PIECE_WIN))
}
