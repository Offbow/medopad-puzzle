package com.medopad.puzzle.ui.views

import android.content.Context
import android.view.View
import android.view.Window
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatDialog
import com.medopad.puzzle.R
import kotlinx.android.synthetic.main.element_template_dialog.*

class TemplateDialog(context: Context) : AppCompatDialog(context) {

    init {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.element_template_dialog)
    }

    fun title(@StringRes title: Int): TemplateDialog {
        return title(context.getString(title))
    }

    fun title(title: String): TemplateDialog {
        template_dialog_title.text = title
        return this
    }

    fun description(@StringRes desc: Int): TemplateDialog {
        return description(context.getString(desc))
    }

    fun description(desc: String): TemplateDialog {
        template_dialog_description.text = desc
        return this
    }

    fun positiveButton(@StringRes positiveText: Int, onPositiveClick: ((TemplateDialog) -> Unit)? = null): TemplateDialog {
        template_dialog_action_pos.visibility = View.VISIBLE
        template_dialog_action_pos.text = context.getString(positiveText)
        template_dialog_action_pos.setOnClickListener {
            dismiss()
            onPositiveClick?.invoke(this)
        }
        return this
    }

    fun negativeButton(@StringRes negativeText: Int, onNegativeClick: ((TemplateDialog) -> Unit)? = null): TemplateDialog {
        template_dialog_action_neg.visibility = View.VISIBLE
        template_dialog_action_neg.text = context.getString(negativeText)
        template_dialog_action_neg.setOnClickListener {
            dismiss()
            onNegativeClick?.invoke(this)
        }
        return this
    }

    fun dismiss(onDismissClick: (TemplateDialog) -> Unit): TemplateDialog {
        setOnDismissListener { onDismissClick(this) }
        return this
    }

}
