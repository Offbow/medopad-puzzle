package com.medopad.puzzle.ui.splash

import android.content.Intent
import android.os.Bundle
import com.medopad.puzzle.arch.BaseActivity
import com.medopad.puzzle.arch.Launcher
import com.medopad.puzzle.ui.puzzle.PuzzleActivity

class SplashActivity : BaseActivity<SplashMvp.Presenter>(), SplashMvp.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = SplashPresenter(this)
        presenter.onCreate()
    }

    override fun startPuzzleScreen() {
        PuzzleActivity.start(this)
        finish()
    }

    companion object {

        @JvmStatic
        fun startAsNew(launcher: Launcher) {
            val intent = Intent(launcher.context(), SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            launcher.launch(intent)
        }
    }
}
