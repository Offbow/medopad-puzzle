package com.medopad.puzzle.ui.splash

import com.medopad.puzzle.arch.BaseMvp

interface SplashMvp : BaseMvp {

    interface View : BaseMvp.View {
        fun startPuzzleScreen()
    }

    interface Presenter : BaseMvp.Presenter {
        fun onCreate()
    }

}
