package com.medopad.puzzle.ui.puzzle

import com.medopad.puzzle.arch.BaseMvp
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearchBoardNode

interface PuzzleMvp : BaseMvp {

    interface View : BaseMvp.View {
        fun setSlidingPuzzle(puzzleMap: String)
        fun setPuzzleSolutionNoFound()
        fun setPuzzleSolution(nodes: List<BreadthFirstSearchBoardNode>, millisTaken: Long)
        fun showSolverProgress(show: Boolean)
    }

    interface Presenter : BaseMvp.Presenter {
        fun onCreate()
        fun onSimpleSolveTapped()
        fun onPuzzleTapped()
        fun onComprehensiveSolveTapped()
    }

}
