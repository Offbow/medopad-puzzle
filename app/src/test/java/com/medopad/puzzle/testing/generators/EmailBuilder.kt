package com.medopad.puzzle.testing.generators

import com.medopad.puzzle.testing.DataGenerator
import com.medopad.puzzle.testing.FixtureBuilder

class EmailBuilder(dataGenerator: DataGenerator) : FixtureBuilder<String>(dataGenerator) {

    internal var output: String? = null

    fun reset(): EmailBuilder {
        output = null
        return this
    }

    override fun build(): String {
        if (output == null) {
            output = dataGenerator.nextEmail()
        }
        return output!!
    }

    fun setEmail(email: String): EmailBuilder {
        this.output = email
        return this
    }

}
