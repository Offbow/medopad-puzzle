package com.medopad.puzzle.ui.login

import com.medopad.puzzle.ui.splash.SplashMvp
import com.medopad.puzzle.ui.splash.SplashPresenter
import com.medopad.puzzle.util.RxSchedulersOverrideRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SplashPresenterTest {
    @Rule
    @JvmField
    var scheduleOverride = RxSchedulersOverrideRule()

    @Mock
    lateinit var view: SplashMvp.View


    private lateinit var splashPresenter: SplashPresenter

    @Before
    fun setup() {
       splashPresenter = SplashPresenter(view)
    }

    @Test
    fun `Test Start Puzzle Screen`() {
        splashPresenter.onCreate()
        verify<SplashMvp.View>(view).startPuzzleScreen()
    }

}
