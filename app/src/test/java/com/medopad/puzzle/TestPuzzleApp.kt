package com.medopad.puzzle

import android.app.Application
import com.medopad.puzzle.testing.TestTree
import timber.log.Timber

class TestPuzzleApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(TestTree())
    }

}
