package com.medopad.puzzle.arch

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable

class DisposablesForLifecycle : LifecycleObserver {
    var onPauseDisposable = CompositeDisposable()
    var onStopDisposable = CompositeDisposable()
    var onDestroyDisposable = CompositeDisposable()

    private enum class UiState {
        CREATE, START, RESUME
    }

    private var uiState = UiState.CREATE

    fun onBeforeCreate() {
        uiState = UiState.CREATE
        if (onDestroyDisposable.isDisposed) {
            onDestroyDisposable = CompositeDisposable()
        }
    }

    fun onBeforeStart() {
        uiState = UiState.START
        if (onStopDisposable.isDisposed) {
            onStopDisposable = CompositeDisposable()
        }
    }

    fun onBeforeResume() {
        uiState = UiState.RESUME
        if (onPauseDisposable.isDisposed) {
            onPauseDisposable = CompositeDisposable()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onAfterPause() {
        onPauseDisposable.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAfterStop() {
        onStopDisposable.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onAfterDestroy() {
        onDestroyDisposable.clear()
    }
}
