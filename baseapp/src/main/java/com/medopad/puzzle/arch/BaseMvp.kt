package com.medopad.puzzle.arch

interface BaseMvp {
    interface View {
        fun finish()
        fun finishWithSuccess()
    }

    interface AndroidView<T : Presenter> : View {
        override fun finish() {
            throw UnsupportedOperationException("Views don't support finish")
        }
        override fun finishWithSuccess() {
            throw UnsupportedOperationException("Views don't support finishWithSuccess")
        }
        val presenter: T
    }

    interface Presenter {
        fun onDestroy()
    }
}
