package com.medopad.puzzle.arch

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

abstract class BaseActivity<T : BaseMvp.Presenter> : AppCompatActivity(), BaseMvp.View, Launcher {
        protected lateinit var presenter: T

    private val disposableView = DisposablesForLifecycle().apply { lifecycle.addObserver(this) }
    private val resultLambdas = mutableMapOf<Int, ActivityResultCallback?>()

    inline fun <reified T: Any> Activity.extra(key: String, default: T? = null) = lazy {
        val value = intent?.extras?.get(key)
        if (value is T) value else default
    }

    inline fun <reified T: Any> Activity.extraNotNull(key: String, default: T? = null) = lazy {
        val value = intent?.extras?.get(key)
        requireNotNull(if (value is T) value else default) { key }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        disposableView.onBeforeCreate()
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        disposableView.onBeforeStart()
        super.onStart()
    }

    override fun onResume() {
        disposableView.onBeforeResume()
        super.onResume()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    fun Disposable.disposeOnPause() {
        this.addTo(disposableView.onPauseDisposable)
    }

    fun Disposable.disposeOnStop() {
        this.addTo(disposableView.onStopDisposable)
    }

    fun Disposable.disposeOnDestroy() {
        this.addTo(disposableView.onDestroyDisposable)
    }

    override fun context() = this

    override fun launch(intent: Intent) {
        startActivity(intent)
    }

    override fun launchForResult(intent: Intent, code: Int, onResult: ActivityResultCallback?) {
        resultLambdas[code] = onResult
        startActivityForResult(intent, code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        resultLambdas[requestCode]?.let {
            it(resultCode, data)
        }
    }

    override fun finishWithSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    fun setTopView(view: View) {
        ViewCompat.setOnApplyWindowInsetsListener(view) { v, insets ->
            v.setPadding(0, insets.systemWindowInsetTop, 0, 0)
            insets.consumeSystemWindowInsets()
        }
    }

    fun setDarkStatusBarIcons() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val flags = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.decorView.systemUiVisibility = flags
        }
    }

    fun setDrawBehindStatusBar() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }
}
