package com.medopad.puzzle.arch

import android.os.Bundle
import androidx.fragment.app.Fragment
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo

abstract class BaseFragment<T : BaseMvp.Presenter> : Fragment(), BaseMvp.View {
    private val disposableView = DisposablesForLifecycle().apply { lifecycle.addObserver(this) }

    inline fun <reified T: Any> Fragment.extra(key: String, default: T? = null) = lazy {
        val value = arguments?.get(key)
        if (value is T) value else default
    }

    inline fun <reified T: Any> Fragment.extraNotNull(key: String, default: T? = null) = lazy {
        val value = arguments?.get(key)
        requireNotNull(if (value is T) value else default) { key }
    }

    override fun onDestroy() {
        finish()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        disposableView.onBeforeCreate()
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        disposableView.onBeforeStart()
        super.onStart()
    }

    override fun onResume() {
        disposableView.onBeforeResume()
        super.onResume()
    }

    fun Disposable.disposeOnPause() {
        this.addTo(disposableView.onPauseDisposable)
    }

    fun Disposable.disposeOnStop() {
        this.addTo(disposableView.onStopDisposable)
    }

    fun Disposable.disposeOnDestroy() {
        this.addTo(disposableView.onDestroyDisposable)
    }

    protected lateinit var presenter: T

    override fun finish() {
        presenter.onDestroy()
    }

    override fun finishWithSuccess() {
        throw UnsupportedOperationException("Fragment don't support finishWithSuccess")
    }
}
