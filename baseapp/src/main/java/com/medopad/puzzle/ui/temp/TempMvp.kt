package com.medopad.puzzle.ui.temp

import com.medopad.puzzle.arch.BaseMvp

interface TempMvp : BaseMvp {

    interface View : BaseMvp.View

    interface AndroidView<T : Presenter> : BaseMvp.AndroidView<T>, View

    interface Presenter : BaseMvp.Presenter
}
