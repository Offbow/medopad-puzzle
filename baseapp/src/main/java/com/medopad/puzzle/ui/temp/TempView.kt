package com.medopad.puzzle.ui.temp

import android.content.Context
import android.util.AttributeSet
import android.view.View

class TempView : View, TempMvp.AndroidView<TempMvp.Presenter> {
    override val presenter by lazy { TempPresenter(this) }

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet?, resStyle: Int) : super(context, attrs, resStyle) {

    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        presenter.onDestroy()
    }
}
