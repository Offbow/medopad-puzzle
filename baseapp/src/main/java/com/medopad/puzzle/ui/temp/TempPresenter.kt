package com.medopad.puzzle.ui.temp

import com.medopad.puzzle.arch.BasePresenter

class TempPresenter(view: TempMvp.View) : BasePresenter<TempMvp.View>(view), TempMvp.Presenter
