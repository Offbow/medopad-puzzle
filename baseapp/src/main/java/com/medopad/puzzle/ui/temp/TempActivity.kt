package com.medopad.puzzle.ui.temp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.medopad.puzzle.arch.BaseActivity

class TempActivity : BaseActivity<TempMvp.Presenter>(), TempMvp.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = TempPresenter(this)
        setContentView(0)

    }

    companion object {
        @JvmStatic
        fun start(context: Activity, requestCode: Int) {
            val intent = Intent(context, TempActivity::class.java)
            context.startActivityForResult(intent, requestCode)
        }
    }
}
