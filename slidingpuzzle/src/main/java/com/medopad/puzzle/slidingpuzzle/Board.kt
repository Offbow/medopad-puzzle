package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.ALLOWED
import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.BLOCKED
import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.WIN
import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.GameMove

open class Board(blockList: List<Block>) {
    protected var occupiedSpaces: Array<IntArray>

    protected var currentGamePieces: List<Block> = blockList

    init {
        //setting up the maximum occupiable spaces with the blocks max maximum height and width values
        val boardHeight = currentGamePieces.map { it.y + it.height }.max()!!
        val boardWidth = currentGamePieces.map { it.x + it.width }.max()!!
        occupiedSpaces = Array(boardHeight) { IntArray(boardWidth) }

        initOccupiedSpaces()
    }

    private fun initOccupiedSpaces() {
        //build initial occupied spaces with id's
        currentGamePieces.forEach { block ->
            for (h in 0 until block.height) {
                for (w in 0 until block.width) {
                    occupiedSpaces[h + block.y][w + block.x] = block.blockId
                }
            }
        }
    }

    /**
     * Returns map of blocks were a null value is a free space.
     *
     * @return List<List<Block?>> an Y,X map of blocks.
     */
    fun getMap(): List<List<Block?>> {
        return occupiedSpaces.mapIndexed { _, row ->
            row.mapIndexed { _, blockId ->
                currentGamePieces.firstOrNull { it.blockId == blockId }
            }
        }
    }

    fun moveBlock(move: GameMove, distance: Int, blockId: Int): MoveResult {
        val block = currentGamePieces.first { it.blockId == blockId }
        val directionCoordinates = getDirectionCoordinates(move, distance, block)

        val movementResult = canBlockMove(directionCoordinates, block.blockType)
        when (movementResult) {
            ALLOWED, WIN -> updateBlockCoordinates(move, distance, block)
            else -> {
            }
        }
        return movementResult
    }

    protected fun getDirectionCoordinates(move: GameMove, distance: Int, block: Block): List<Pair<Int, Int>> {
        //Build a list of coordinates the block is proposed to move
        val results = mutableListOf<Pair<Int, Int>>()
        for (steps in 1..distance) {
            when (move) {
                GameMove.UP -> {
                    for (w in 0 until block.width) {
                        results.add(Pair(block.getTopCoordinate(steps), block.x + w))
                    }
                }
                GameMove.DOWN -> {
                    for (w in 0 until block.width) {
                        results.add(Pair(block.getBottomCoordinate(steps), block.x + w))
                    }
                }
                GameMove.LEFT -> {
                    for (h in 0 until block.height) {
                        results.add(Pair(block.y + h, block.getLeftCoordinate(steps)))
                    }
                }
                GameMove.RIGHT -> {
                    for (h in 0 until block.height) {
                        results.add(Pair(block.y + h, block.getRightCoordinate(steps)))
                    }
                }
            }
        }
        return results
    }

    protected fun canBlockMove(coordinates: List<Pair<Int, Int>>, blockType: Block.BlockType): MoveResult {
        //calculate if move is possible

        //is the move outside the bounds of what is possible within the boards size
        val isMoveOutOfBounds = coordinates.any {
            it.first >= occupiedSpaces.size || it.second >= occupiedSpaces.first().size ||
                    it.first < 0 || it.second < 0
        }
        if (isMoveOutOfBounds) {
            return BLOCKED
        }
        //get ids of moving locations
        val coordinateResult = coordinates
                .map { occupiedSpaces[it.first][it.second] }
        return when {
            blockType == Block.BlockType.PIECE_WIN && coordinateResult.all { it == Block.GOAL_ID } -> WIN
            blockType == Block.BlockType.PIECE_WIN && coordinateResult.subList(0, coordinateResult.size / 2).all { it == Block.BLANK_ID } &&
                    coordinateResult.subList(coordinateResult.size / 2, coordinateResult.size).all { it == Block.GOAL_ID } -> WIN
            coordinateResult.all { it == Block.BLANK_ID } -> ALLOWED
            else -> BLOCKED
        }
    }

    protected fun updateBlockCoordinates(move: GameMove, distance: Int, block: Block) {
        //updating only the affected spaces instead of rebuilding

        //Clear blocks current position
        for (h in 0 until block.height) {
            for (w in 0 until block.width) {
                occupiedSpaces[h + block.y][w + block.x] = Block.BLANK_ID
            }
        }

        //update block x or y to new location
        when (move) {
            GameMove.UP -> block.y -= distance
            GameMove.DOWN -> block.y += distance
            GameMove.LEFT -> block.x -= distance
            GameMove.RIGHT -> block.x += distance
        }

        //add blocks new position
        for (h in 0 until block.height) {
            for (w in 0 until block.width) {
                occupiedSpaces[h + block.y][w + block.x] = block.blockId
            }
        }
    }

    enum class MoveResult {
        BLOCKED, ALLOWED, WIN
    }

}