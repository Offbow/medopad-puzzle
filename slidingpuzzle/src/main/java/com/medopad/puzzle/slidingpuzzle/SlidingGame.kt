package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.ALLOWED
import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.BLOCKED
import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.WIN
import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.BLANK_ID
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.GOAL_ID
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.WALL_ID
import com.medopad.puzzle.slidingpuzzle.model.GameInvalidMovementState
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.model.GameNoMovementState
import com.medopad.puzzle.slidingpuzzle.model.GameProgressState
import com.medopad.puzzle.slidingpuzzle.model.GameState
import com.medopad.puzzle.slidingpuzzle.model.GameWonState
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

class SlidingGame(blockList: List<Block> = DEFAULT_PIECES) {

    private val gameStateSubject = PublishSubject.create<GameState>()

    private var currentMoves: Int = 0

    private val gameBoard: Board = Board(blockList)

    /**
     * A Flowable of the current games state used to identify the result of a move performed using [performMove()].
     * @return Flowable<GameState> Flowable on the game state
     */
    fun getGameStateFlowable(): Flowable<GameState> = gameStateSubject.share().toFlowable(BackpressureStrategy.LATEST)

    /**
     * Performs the move as instructed on the game
     * @param [move] The direction of the move
     * @param [blockId] The block id of the block that is being moved
     */
    fun performMove(move: GameMove, distance: Int, blockId: Int) {
        if (blockId == BLANK_ID || blockId == WALL_ID || distance < 1 || distance > 2) {
            gameStateSubject.onNext(GameInvalidMovementState(currentMoves))
            return
        }

        currentMoves += 1
        val moveResult = gameBoard.moveBlock(move, distance, blockId)
        gameStateSubject.onNext(
                when (moveResult) {
                    BLOCKED -> GameNoMovementState(currentMoves)
                    ALLOWED -> GameProgressState(currentMoves)
                    WIN -> GameWonState(currentMoves)
                })
    }

    /**
     * Returns map of blocks were a null value is a free space.
     *
     * @return List<List<Block?>> an Y,X map of blocks.
     */
    fun getBoardMap(): List<List<Block?>> {
        return gameBoard.getMap()
    }

    companion object {

        val DEFAULT_PIECES = listOf(
                Block(WALL_ID, 1, 6, 0, 0, 'X', Block.BlockType.WALL),
                Block(WALL_ID, 6, 1, 0, 1, 'X', Block.BlockType.WALL),
                Block(WALL_ID, 6, 1, 5, 1, 'X', Block.BlockType.WALL),
                Block(WALL_ID, 1, 1, 1, 6, 'X', Block.BlockType.WALL),
                Block(WALL_ID, 1, 1, 4, 6, 'X', Block.BlockType.WALL),
                Block(GOAL_ID, 1, 2, 2, 6, 'Z', Block.BlockType.WALL_GOAL),
                Block(3, 2, 1, 1, 1, 'a', Block.BlockType.PIECE),
                Block(4, 2, 1, 4, 1, 'c', Block.BlockType.PIECE),
                Block(5, 2, 1, 1, 3, 'd', Block.BlockType.PIECE),
                Block(6, 2, 1, 4, 3, 'f', Block.BlockType.PIECE),
                Block(7, 1, 2, 2, 3, 'e', Block.BlockType.PIECE),
                Block(8, 1, 1, 2, 4, 'g', Block.BlockType.PIECE),
                Block(9, 1, 1, 3, 4, 'h', Block.BlockType.PIECE),
                Block(10, 1, 1, 1, 5, 'i', Block.BlockType.PIECE),
                Block(11, 1, 1, 4, 5, 'j', Block.BlockType.PIECE),
                Block(12, 2, 2, 2, 1, 'b', Block.BlockType.PIECE_WIN)
        )
    }
}
