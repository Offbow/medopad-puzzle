package com.medopad.puzzle.slidingpuzzle.model

import com.medopad.puzzle.slidingpuzzle.model.GameMove.DOWN
import com.medopad.puzzle.slidingpuzzle.model.GameMove.LEFT
import com.medopad.puzzle.slidingpuzzle.model.GameMove.RIGHT
import com.medopad.puzzle.slidingpuzzle.model.GameMove.UP

enum class GameMove {
    UP, DOWN, LEFT, RIGHT
}

val gameMovePermutations: Array<Array<GameMove>> = arrayOf(arrayOf(UP, DOWN, LEFT, RIGHT),
        arrayOf(DOWN, UP, LEFT, RIGHT),
        arrayOf(LEFT, UP, DOWN, RIGHT),
        arrayOf(UP, LEFT, DOWN, RIGHT),
        arrayOf(DOWN, LEFT, UP, RIGHT),
        arrayOf(LEFT, DOWN, UP, RIGHT),
        arrayOf(LEFT, DOWN, RIGHT, UP),
        arrayOf(DOWN, LEFT, RIGHT, UP),
        arrayOf(RIGHT, LEFT, DOWN, UP),
        arrayOf(LEFT, RIGHT, DOWN, UP),
        arrayOf(DOWN, RIGHT, LEFT, UP),
        arrayOf(RIGHT, DOWN, LEFT, UP),
        arrayOf(RIGHT, UP, LEFT, DOWN),
        arrayOf(UP, RIGHT, LEFT, DOWN),
        arrayOf(LEFT, RIGHT, UP, DOWN),
        arrayOf(RIGHT, LEFT, UP, DOWN),
        arrayOf(UP, LEFT, RIGHT, DOWN),
        arrayOf(LEFT, UP, RIGHT, DOWN),
        arrayOf(DOWN, UP, RIGHT, LEFT),
        arrayOf(UP, DOWN, RIGHT, LEFT),
        arrayOf(RIGHT, DOWN, UP, LEFT),
        arrayOf(DOWN, RIGHT, UP, LEFT),
        arrayOf(UP, RIGHT, DOWN, LEFT),
        arrayOf(RIGHT, UP, DOWN, LEFT))