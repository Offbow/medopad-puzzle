package com.medopad.puzzle.slidingpuzzle.model

class Block(val blockId: Int,
            val height: Int,
            val width: Int,
            var x: Int,
            var y: Int,
            val blockCharacter: Char,
            val blockType: BlockType) {

    fun getTopCoordinate(distance: Int): Int = y - distance

    fun getBottomCoordinate(distance: Int): Int = (y + (height - 1)) + distance

    fun getLeftCoordinate(distance: Int): Int = x - distance

    fun getRightCoordinate(distance: Int): Int = (x + (width - 1)) + distance

    enum class BlockType {
        WALL, WALL_GOAL, PIECE, PIECE_WIN
    }

    companion object {
        const val BLANK_ID = 0
        const val WALL_ID = 1
        const val GOAL_ID = 2
    }

    fun copy(blockId: Int = this.blockId,
             height: Int = this.height,
             width: Int = this.width,
             x: Int = this.x,
             y: Int = this.y,
             blockCharacter: Char = this.blockCharacter,
             blockType: BlockType = this.blockType): Block = Block(blockId, height, width, x, y, blockCharacter, blockType)
}