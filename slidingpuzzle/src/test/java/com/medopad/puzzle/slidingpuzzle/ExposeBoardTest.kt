package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.GameMove

internal class ExposeBoardTest(blockList: List<Block>) : Board(blockList) {

    internal fun getDirectionCoordinatesTEST(move: GameMove, distance: Int, block: Block): List<Pair<Int, Int>> = getDirectionCoordinates(move, distance, block)

    internal fun canBlockMoveTEST(coordinates: List<Pair<Int, Int>>, blockType: Block.BlockType): MoveResult = canBlockMove(coordinates, blockType)

    internal fun updateBlockCoordinatesTEST(move: GameMove, distance: Int, block: Block) = updateBlockCoordinates(move, distance, block)
    
}