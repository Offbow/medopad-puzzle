package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.BLANK_ID
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.WALL_ID
import com.medopad.puzzle.slidingpuzzle.model.GameInvalidMovementState
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.model.GameNoMovementState
import com.medopad.puzzle.slidingpuzzle.model.GameProgressState
import com.medopad.puzzle.slidingpuzzle.model.GameState
import com.medopad.puzzle.slidingpuzzle.model.GameWonState
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import io.reactivex.subscribers.TestSubscriber
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.TimeUnit

class TestSlidingGame {

    @Test
    fun `Test Sliding Game map construction full`() {
        //GIVEN
        val slidingGame = SlidingGame(TestConsts.VALID_BOARD)
        //WHEN
        val map = slidingGame.getBoardMap()
        val printedMap = BoardMapUtil.formatBoardMap(map)
        //THEN
        Assert.assertEquals("Constructed Board Equal", TestConsts.VALID_BOARD_PRINTOUT, printedMap)
    }

    @Test
    fun `Test state update Invalid`() {
        //GIVEN
        val testSubscriber = TestSubscriber<GameState>()
        val slidingGame = SlidingGame(TestConsts.VALID_BOARD)

        //WHEN
        slidingGame.getGameStateFlowable().subscribe(testSubscriber)
        slidingGame.performMove(GameMove.UP, 1, BLANK_ID)
        slidingGame.performMove(GameMove.UP, 1, WALL_ID)
        slidingGame.performMove(GameMove.UP, 0, 9)
        slidingGame.performMove(GameMove.UP, 3, 9)

        //THEN
        testSubscriber.awaitTerminalEvent(50, TimeUnit.MILLISECONDS)
        testSubscriber.assertNoErrors()
        testSubscriber.assertValueCount(4)

        Assert.assertEquals("Check for Invalid move state blank space", GameInvalidMovementState(0).javaClass, testSubscriber.values()[0].javaClass)
        Assert.assertEquals("Check Invalid state does not increment counter", 0, (testSubscriber.values()[0] as GameInvalidMovementState).currentMoves)

        Assert.assertEquals("Check for Invalid move state wall", GameInvalidMovementState(0).javaClass, testSubscriber.values()[1].javaClass)
        Assert.assertEquals("Check Invalid state does not increment counter", 0, (testSubscriber.values()[0] as GameInvalidMovementState).currentMoves)

        Assert.assertEquals("Check for Invalid move state 0 distance", GameInvalidMovementState(0).javaClass, testSubscriber.values()[2].javaClass)
        Assert.assertEquals("Check Invalid state does not increment counter", 0, (testSubscriber.values()[0] as GameInvalidMovementState).currentMoves)

        Assert.assertEquals("Check for Invalid move state 3 distance", GameInvalidMovementState(0).javaClass, testSubscriber.values()[3].javaClass)
        Assert.assertEquals("Check Invalid state does not increment counter", 0, (testSubscriber.values()[0] as GameInvalidMovementState).currentMoves)
    }

    @Test
    fun `Test state update progress Movement`() {
        //GIVEN
        val testSubscriber = TestSubscriber<GameState>()
        val slidingGame = SlidingGame(TestConsts.VALID_BOARD)

        //WHEN
        slidingGame.getGameStateFlowable().subscribe(testSubscriber)
        slidingGame.performMove(GameMove.DOWN, 1, 9)

        //THEN
        testSubscriber.awaitTerminalEvent(50, TimeUnit.MILLISECONDS)
        testSubscriber.assertNoErrors()
        testSubscriber.assertValueCount(1)

        Assert.assertEquals("Check for progress move state", GameProgressState(1).javaClass, testSubscriber.values()[0].javaClass)
        Assert.assertEquals("Check progress state does increment counter", 1, (testSubscriber.values()[0] as GameProgressState).currentMoves)
    }

    @Test
    fun `Test state update No Movement`() {
        //GIVEN
        val testSubscriber = TestSubscriber<GameState>()
        val slidingGame = SlidingGame(TestConsts.VALID_BOARD)

        //WHEN
        slidingGame.getGameStateFlowable().subscribe(testSubscriber)
        slidingGame.performMove(GameMove.DOWN, 1, 12)

        //THEN
        testSubscriber.awaitTerminalEvent(50, TimeUnit.MILLISECONDS)
        testSubscriber.assertNoErrors()
        testSubscriber.assertValueCount(1)

        Assert.assertEquals("Check for no move state", GameNoMovementState(1).javaClass, testSubscriber.values()[0].javaClass)
        Assert.assertEquals("Check no move state does increment counter", 1, (testSubscriber.values()[0] as GameNoMovementState).currentMoves)
    }

    @Test
    fun `Test state update won`() {
        //GIVEN
        val testSubscriber = TestSubscriber<GameState>()
        val slidingGame = SlidingGame(TestConsts.VALID_BASIC_BOARD)

        //WHEN
        slidingGame.getGameStateFlowable().subscribe(testSubscriber)
        slidingGame.performMove(GameMove.DOWN, 1, 12)

        //THEN
        testSubscriber.awaitTerminalEvent(50, TimeUnit.MILLISECONDS)
        testSubscriber.assertNoErrors()
        testSubscriber.assertValueCount(1)

        Assert.assertEquals("Check for won state", GameWonState(1).javaClass, testSubscriber.values()[0].javaClass)
        Assert.assertEquals("Check won state does return total counter", 1, (testSubscriber.values()[0] as GameWonState).totalMoves)
    }

}