package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.model.Block

object TestConsts {
    //    XXXXXX
    //    XabbcX
    //    XabbcX
    //    XdeefX
    //    XdghfX
    //    Xi00jX
    //    XXZZXX
    val VALID_BOARD: List<Block> = listOf(
            Block(Block.WALL_ID, 1, 6, 0, 0, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 6, 1, 0, 1, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 6, 1, 5, 1, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 1, 1, 1, 6, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 1, 1, 4, 6, 'X', Block.BlockType.WALL),
            Block(Block.GOAL_ID, 1, 2, 2, 6, 'Z', Block.BlockType.WALL_GOAL),
            Block(3, 2, 1, 1, 1, 'a', Block.BlockType.PIECE),
            Block(4, 2, 1, 4, 1, 'c', Block.BlockType.PIECE),
            Block(5, 2, 1, 1, 3, 'd', Block.BlockType.PIECE),
            Block(6, 2, 1, 4, 3, 'f', Block.BlockType.PIECE),
            Block(7, 1, 2, 2, 3, 'e', Block.BlockType.PIECE),
            Block(8, 1, 1, 2, 4, 'g', Block.BlockType.PIECE),
            Block(9, 1, 1, 3, 4, 'h', Block.BlockType.PIECE),
            Block(10, 1, 1, 1, 5, 'i', Block.BlockType.PIECE),
            Block(11, 1, 1, 4, 5, 'j', Block.BlockType.PIECE),
            Block(12, 2, 2, 2, 1, 'b', Block.BlockType.PIECE_WIN))
        get() = field.map { it.copy() }

    val VALID_BOARD_PRINTOUT = "XXXXXX\nXabbcX\nXabbcX\nXdeefX\nXdghfX\nXi00jX\nXXZZXX\n"

    //  XXXX
    //  XbbX
    //  XbbX
    //  XZZX
    val VALID_BASIC_BOARD = listOf(
            Block(Block.WALL_ID, 1, 4, 0, 0, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 3, 1, 0, 1, 'X', Block.BlockType.WALL),
            Block(Block.WALL_ID, 3, 1, 3, 1, 'X', Block.BlockType.WALL),
            Block(Block.GOAL_ID, 1, 2, 1, 3, 'Z', Block.BlockType.WALL_GOAL),
            Block(12, 2, 2, 1, 1, 'b', Block.BlockType.PIECE_WIN))
        get() = field.map { it.copy() }

    val VALID_BASIC_BOARD_PRINTOUT = "XXXX\nXbbX\nXbbX\nXZZX\n"
    val VALID_BASIC_BOARD_PRINTOUT_WON = "XXXX\nX00X\nXbbX\nXbbX\n"

}