package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BASIC_BOARD
import com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BASIC_BOARD_PRINTOUT_WON
import com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BOARD
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import org.junit.Assert
import org.junit.Test

class TestBoardInternal {

    @Test
    fun `Test Getting Direction Coordinates`() {
        //GIVEN
        val coordsUP = listOf(2 to 2, 2 to 3, 1 to 2, 1 to 3)
        val coordsDOWN = listOf(4 to 2, 4 to 3, 5 to 2, 5 to 3)
        val coordsLEFT = listOf((3 to 1), (3 to 0))
        val coordsRIGHT = listOf((3 to 4), (3 to 5))

        val exposedBoard = ExposeBoardTest(VALID_BOARD)
        val testBlock = VALID_BOARD[10]
        //WHEN
        val directionCoordinatesUP = exposedBoard.getDirectionCoordinatesTEST(GameMove.UP, 2, testBlock)
        val directionCoordinatesDOWN = exposedBoard.getDirectionCoordinatesTEST(GameMove.DOWN, 2, testBlock)
        val directionCoordinatesLEFT = exposedBoard.getDirectionCoordinatesTEST(GameMove.LEFT, 2, testBlock)
        val directionCoordinatesRIGHT = exposedBoard.getDirectionCoordinatesTEST(GameMove.RIGHT, 2, testBlock)

        //THEN
        Assert.assertEquals("Coordinates up", coordsUP, directionCoordinatesUP)
        Assert.assertEquals("Coordinates down", coordsDOWN, directionCoordinatesDOWN)
        Assert.assertEquals("Coordinates left", coordsLEFT, directionCoordinatesLEFT)
        Assert.assertEquals("Coordinates right", coordsRIGHT, directionCoordinatesRIGHT)
    }

    @Test
    fun `Test result of can move blocked and allowed `() {
        //GIVEN
        val coordsUP = listOf((4 to 4), (3 to 4))
        val coordsDOWN = listOf((6 to 4), (7 to 4))
        val coordsLEFT = listOf((5 to 3), (5 to 2))
        val coordsRIGHT = listOf((5 to 5), (5 to 6))

        val exposedBoard = ExposeBoardTest(VALID_BOARD)
        val testBlock = VALID_BOARD[14]
        //WHEN
        val moveResultUP = exposedBoard.canBlockMoveTEST(coordsUP, testBlock.blockType)
        val moveResultDOWN = exposedBoard.canBlockMoveTEST(coordsDOWN, testBlock.blockType)
        val moveResultLEFT = exposedBoard.canBlockMoveTEST(coordsLEFT, testBlock.blockType)
        val moveResultRIGHT = exposedBoard.canBlockMoveTEST(coordsRIGHT, testBlock.blockType)

        //THEN
        Assert.assertEquals("Coordinates up", Board.MoveResult.BLOCKED, moveResultUP)
        Assert.assertEquals("Coordinates down", Board.MoveResult.BLOCKED, moveResultDOWN)
        Assert.assertEquals("Coordinates left", Board.MoveResult.ALLOWED, moveResultLEFT)
        Assert.assertEquals("Coordinates right", Board.MoveResult.BLOCKED, moveResultRIGHT)
    }

    @Test
    fun `Test result of can move win `() {
        val coordsDOWN = listOf((3 to 1), (3 to 2))

        //GIVE
        val exposedBoard = ExposeBoardTest(VALID_BASIC_BOARD)
        val testBlock = VALID_BASIC_BOARD[4]

        //WHEN
        val moveResultDOWN = exposedBoard.canBlockMoveTEST(coordsDOWN, testBlock.blockType)

        //THEN
        Assert.assertEquals("Coordinates up", Board.MoveResult.WIN, moveResultDOWN)

    }

    @Test
    fun `Test result of move `() {
        val coordsDOWN = listOf((3 to 1), (3 to 2))

        //GIVE
        val board = VALID_BASIC_BOARD
        val exposedBoard = ExposeBoardTest(board)
        val testBlock = board[4]

        //WHEN
        val startingMap = exposedBoard.getMap()
        exposedBoard.updateBlockCoordinatesTEST(GameMove.DOWN, 1, testBlock)

        //THEN
        Assert.assertNotEquals("Map is not the same", startingMap, BoardMapUtil.formatBoardMap(exposedBoard.getMap()))
        Assert.assertEquals("Map correctly updated", VALID_BASIC_BOARD_PRINTOUT_WON, BoardMapUtil.formatBoardMap(exposedBoard.getMap()))

    }
}