# Puzzle test
A test project to display, play and solve a puzzle board.

![Sample screenshot](demo/demo.png)

### Example Board
The minimum number of steps this project takes to complete the example board is 98.
```
XXXXXX
XabbcX
XabbcX
XdeefX
XdghfX
Xi  jX
XXZZXX
```

### To run
This is an Android project built in Android studio. To run, download and open this project in Android studio
OR use the apk in the "demo" folder "PuzzleApp_1_standard_release.apk".

### Limitations
-Speed
A single Breadth First Search is slow on a mobile device and can take some time, normally 20 to 30 seconds. This is due to the memory and CPU limits for apps on Android.
An easy solution to this is to move the project to a project on a PC/Server, where the search would be performed in "moments".
To speed up the process on Android, the maps and other supporting objects of the game can be slimmed down to improve its memory footprint. If the process remains slow then a background service that allows the user to navigate away could be implemented.

-Only implements Breadth First Search
-Configuration
There is limited configuration for the search, even with a set of 24 move permutations the lowest number of steps is 98. 
Another configurable option would be to continue searching on each permutation for N times to search for more possible solutions (removing any node that hits the same or greater tree depth).


