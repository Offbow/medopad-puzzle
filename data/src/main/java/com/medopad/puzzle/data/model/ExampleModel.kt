package com.medopad.puzzle.data.model

data class ExampleModel(val id: Int, val names: List<String>) {

    //instead of using copy() wrapper functions should be created
    //as these explain intent and help ensure incorrect copies won't be made

    fun withNames(names: List<String>): ExampleModel {
        return copy(names = names)
    }

    fun plusName(name: String): ExampleModel {
        return copy(names = names.plus(name))
    }

    fun minusName(name: String): ExampleModel {
        return copy(names = names.minus(name))
    }

    fun withNoNames(): ExampleModel {
        return copy(names = listOf())
    }

    override fun hashCode(): Int {
        return id
    }

    override fun equals(other: Any?): Boolean {
        if (other is ExampleModel) {
            return id == other.id
        } else {
            return false
        }
    }
}
