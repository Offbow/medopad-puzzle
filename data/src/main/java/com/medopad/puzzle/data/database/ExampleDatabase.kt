package com.medopad.puzzle.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.medopad.puzzle.data.model.ExampleDbModel

@Database(entities = [ExampleDbModel::class], version = 1)
internal abstract class ExampleDatabase : RoomDatabase() {
    abstract fun exampleDao(): ExampleDao
}
