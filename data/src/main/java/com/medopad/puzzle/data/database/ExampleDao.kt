package com.medopad.puzzle.data.database

import androidx.room.*
import com.medopad.puzzle.data.model.ExampleDbModel
import io.reactivex.Flowable

internal const val EXAMPLE_TABLE_NAME = "profiles"
internal const val EXAMPLE_ID = "_id"
internal const val EXAMPLE_NAME = "name"
internal const val EXAMPLE_PHONE = "phone"
internal const val EXAMPLE_EMAIL = "email"

@Dao
internal interface ExampleDao {
    @Query("SELECT * FROM $EXAMPLE_TABLE_NAME ORDER BY $EXAMPLE_NAME")
    fun getAllExample(): Flowable<List<ExampleDbModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg contact: ExampleDbModel): List<Long>

    @Delete
    fun delete(contact: ExampleDbModel): Int

    @Query("SELECT * FROM $EXAMPLE_TABLE_NAME WHERE $EXAMPLE_ID = :id LIMIT 1")
    fun getExampleById(id: Long): Flowable<ExampleDbModel>

    @Query("DELETE FROM $EXAMPLE_TABLE_NAME")
    fun deleteTableContent()

}
