package com.medopad.puzzle.data.api

import com.medopad.puzzle.data.model.ExampleModel
import com.medopad.puzzle.data.model.TokenRequest
import com.medopad.puzzle.data.model.TokenResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ResourceService {
    @POST("oauth/token")
    fun getTokenSingle(@Body tokenRequest: TokenRequest): Single<Response<TokenResponse>>

    @GET("v1/accounts")
    fun getAccountsSingle(): Single<Response<List<ExampleModel>>>

    @GET("v1/{subdomain}/resources")
    fun getResources(@Path("subdomain") subdomain: String): Single<Response<List<ExampleModel>>>

    @GET("v1/{subdomain}/resources/{id}")
    fun getResource(@Path("subdomain") subdomain: String, @Path("id") id: Int): Single<ExampleModel>
}
