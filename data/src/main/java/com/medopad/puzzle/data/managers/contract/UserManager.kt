package com.medopad.puzzle.data.managers.contract

import io.reactivex.Flowable

interface UserManager {
    val tokenObservable: Flowable<String>

    var token: String

}
