package com.medopad.puzzle.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.medopad.puzzle.data.database.EXAMPLE_EMAIL
import com.medopad.puzzle.data.database.EXAMPLE_ID
import com.medopad.puzzle.data.database.EXAMPLE_NAME
import com.medopad.puzzle.data.database.EXAMPLE_PHONE
import com.medopad.puzzle.data.database.EXAMPLE_TABLE_NAME

@Entity(tableName = EXAMPLE_TABLE_NAME, indices = [Index(EXAMPLE_EMAIL, unique = true)])
data class ExampleDbModel(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = EXAMPLE_ID)
        val id: Long? = null,

        @ColumnInfo(name = EXAMPLE_NAME)
        val name: String,

        @ColumnInfo(name = EXAMPLE_EMAIL)
        val email: String,

        @ColumnInfo(name = EXAMPLE_PHONE)
        val phone: String
)
