package com.medopad.puzzle.data.managers

import com.medopad.puzzle.data.managers.contract.DataManager
import com.medopad.puzzle.data.managers.contract.ManagerResult
import com.medopad.puzzle.data.model.TokenRequest
import io.reactivex.Single

class DataManagerImpl() : DataManager {
    override fun getExampleWithResult(tokenRequest: TokenRequest): Single<ManagerResult> {
        return Single.just(ManagerResult.SUCCESS)
    }

}
