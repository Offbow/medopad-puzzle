package com.medopad.puzzle.data.model

import com.google.gson.annotations.SerializedName

data class TokenRequest(
        val username: String,
        val password: String,
        @SerializedName("grant_type")
        val grantType: String = "password")
