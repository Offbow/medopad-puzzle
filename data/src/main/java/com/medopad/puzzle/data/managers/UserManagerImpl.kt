package com.medopad.puzzle.data.managers

import com.f2prateek.rx.preferences2.Preference
import com.medopad.puzzle.arch.PreferenceModule
import com.medopad.puzzle.data.managers.contract.UserManager
import com.medopad.puzzle.ext.rx.asFlowable
import io.reactivex.Flowable
import io.reactivex.Observable

class UserManagerImpl constructor(private val tokenPreference: Preference<String> = PreferenceModule.tokenPref) : UserManager {

    override val tokenObservable: Flowable<String>
        get() = tokenPreference.asFlowable()

    override var token: String
        get() = tokenPreference.get()
        set(token) = tokenPreference.set(token)

}
