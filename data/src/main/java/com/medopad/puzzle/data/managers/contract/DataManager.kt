package com.medopad.puzzle.data.managers.contract

import com.medopad.puzzle.data.model.TokenRequest
import io.reactivex.Single

interface DataManager {

    fun getExampleWithResult(tokenRequest: TokenRequest): Single<ManagerResult>
}

enum class ManagerResult {
    SUCCESS, NETWORK_ERROR, DEVICE_ERROR, ERROR
}
