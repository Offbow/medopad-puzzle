package com.medopad.puzzle.ext.rx

import io.reactivex.Single

fun List<Long>.wrap(): Single<List<Long>> {
    return Single.fromCallable { this }
}
