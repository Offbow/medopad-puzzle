package com.medopad.puzzle.ext.android

import android.graphics.Canvas

fun Canvas.alter(func: () -> Unit) {
    save()
    func()
    restore()
}
