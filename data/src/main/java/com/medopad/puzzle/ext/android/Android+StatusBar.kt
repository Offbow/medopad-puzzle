package com.medopad.puzzle.ext.android

import android.app.Activity
import android.os.Build
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.Fragment

fun Activity.setStatusBarColorWithColor(@ColorInt marshmallow: Int, @ColorInt lollipop: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        window.statusBarColor = marshmallow
        if (ColorUtils.calculateLuminance(marshmallow) > 0.5) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    } else {
        window.statusBarColor = lollipop
    }
}

fun Fragment.setStatusBarColorWithColor(@ColorInt marshmallow: Int, @ColorInt lollipop: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        activity!!.window.statusBarColor = marshmallow
        if (ColorUtils.calculateLuminance(marshmallow) > 0.5) {
            activity!!.window.decorView.systemUiVisibility = activity!!.window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    } else {
        activity!!.window.statusBarColor = lollipop
    }
}

fun Activity.setStatusBarColorById(@ColorRes marshmallow: Int, @ColorRes lollipop: Int) {
    setStatusBarColorWithColor(getCompatColor(marshmallow), getCompatColor(lollipop))
}

fun Fragment.setStatusBarColorById(@ColorRes marshmallow: Int, @ColorRes lollipop: Int) {
    setStatusBarColorWithColor(activity!!.getCompatColor(marshmallow), activity!!.getCompatColor(lollipop))
}
