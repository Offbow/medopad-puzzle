package com.medopad.puzzle.ext.rx


import com.f2prateek.rx.preferences2.Preference
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

fun <T> Preference<T>.asFlowable(): Flowable<T> {
    return this.asObservable().toFlowable(BackpressureStrategy.LATEST)
}
