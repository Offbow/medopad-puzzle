package com.medopad.puzzle.arch

import android.annotation.SuppressLint
import android.preference.PreferenceManager
import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import org.joda.time.DateTime

@SuppressLint("CheckResult") //incorrectly detecting results ignored for rxSharedPrefs
object PreferenceModule {

    private val sharedPrefs by lazy {
        PreferenceManager.getDefaultSharedPreferences(AppModule.application)
    }

    private val rxSharedPrefs by lazy {
        RxSharedPreferences.create(sharedPrefs)
    }

    val tokenPref by lazy {
        rxSharedPrefs.getString("token", "")
    }

    val lastUpdatedPref by lazy {
        rxSharedPrefs.getObject("last_update.string", DateTime.parse("1970-01-01T00:00:00Z"), object : Preference.Converter<DateTime> {
            override fun deserialize(serialized: String) = DateTime.parse(serialized)
            override fun serialize(value: DateTime) = value.toString()
        })
    }
}
