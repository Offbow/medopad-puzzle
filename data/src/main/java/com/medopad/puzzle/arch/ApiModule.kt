package com.medopad.puzzle.arch

import android.text.TextUtils
import com.medopad.puzzle.arch.DataModule.userManager
import com.medopad.puzzle.data.BuildConfig
import com.medopad.puzzle.data.api.ResourceService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

internal object ApiModule {

    internal val resourceGuruService by lazy {
        retrofit.create(ResourceService::class.java)
    }

    private val retrofit by lazy {
        Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    internal val okHttpClient by lazy {
        OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor { message -> Timber.d(message) }
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .addNetworkInterceptor { chain ->
                    var request = chain.request()
                    if (!TextUtils.isEmpty(userManager.token)) {
                        request = request.newBuilder()
                                .addHeader("Authorization", "Bearer " + userManager.token)
                                .build()
                    }

                    chain.proceed(request)
                }
                .build()
    }
}
