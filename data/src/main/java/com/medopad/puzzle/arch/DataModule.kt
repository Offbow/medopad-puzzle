package com.medopad.puzzle.arch

import com.medopad.puzzle.data.managers.DataManagerImpl
import com.medopad.puzzle.data.managers.RxConnectivity
import com.medopad.puzzle.data.managers.UserManagerImpl

object DataModule {

    val dataManager by lazy {
        DataManagerImpl()
    }

    val userManager by lazy {
        UserManagerImpl()
    }

    val rxConnectivity by lazy {
        RxConnectivity()
    }


}
